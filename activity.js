// 2. Create a database of hotel with a collection of rooms.

//3. Insert a single room (insertOne method) with the following details:

db.hotels.insertOne({
	"name": "Single",
	"accomodates": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"rooms_available": 10,
	"isAvailable": false
})

// 4. Insert multiple rooms (insertMany method) with the following

db.hotels.insertMany([{
	"name": "Queen",
	"accomodates": 4,
	"price": 4000,
	"description": "A sroom with a queen size bed perfect for a simple getaway",
	"rooms_available": 15,
	"isAvailable": false
},



	{"name": "Double",
	"accomodates": 3,
	"price": 2000,
	"description": "A room fit for a small family going on a vacation",
	"rooms_available": 5,
	"isAvailable": false


}])

//5. Use the find method to search for a room with the name double.

db.hotels.find({"name": "Double"})


//6. Use the updateOne method to update the queen room and set the
available rooms to 0.

db.hotels.updateOne({"name": "Double"},
		{
			$set: {
				"name": "Queen",
				"accomodates": 4,
				"price": 4000,
				"description": "A sroom with a queen size bed perfect for a simple getaway",
				"rooms_available": 0,
				"isAvailable": false

				}
		})

//7. Use the deleteMany method rooms to delete all rooms that have 0

db.hotels.deleteMany({"isAvailable": false})